var express = require("express");
var app = express();

app.use(express.static("./public"));



// API DOC FOR EXCEL REPORT


/**
 * @api {get} /api/getIssueReport Get excel report of all issues.
 * @apiGroup EXCEL REPORT
 * 
 * @apiSuccessExample {json} Success.
 *    HTTP/1.1 200 OK
 *   
 * {
    "message": "Issue report created",
    "link": "http://localhost:3002/issue1583219773378.xlsx"
}
 * 
 *  @apiErrorExample {json}  Error 500:
 *     HTTP/1.1 500 SERVER ERROR
 *    
 * {
    "message": "something went wrong"
}
 */


// API DOC FOR USER GROUP

/**
 * @api {get} /api/getIssues/:userId?offset=3 Get all issue of user group by user id.
 * @apiParam (params) {Id} userId user id
 * @apiParam (query) {Number} offset offset for pagination
 * @apiGroup USER GROUP
 * 
 * @apiSuccessExample {json} Success.
 *    HTTP/1.1 200 OK
 *   {
    "issues": [
        {
            "id": 14,
            "project": {
                "id": 1,
                "name": "test_redmine"
            },
            "tracker": {
                "id": 1,
                "name": "Bug"
            },
            "status": {
                "id": 1,
                "name": "New"
            },
            "priority": {
                "id": 2,
                "name": "Normal"
            },
            "author": {
                "id": 141,
                "name": "sandeep@gmail.com sandeep@gmail.com"
            },
            "subject": "issue by sandeep",
            "description": "",
            "start_date": "2020-01-09",
            "due_date": null,
            "done_ratio": 0,
            "is_private": false,
            "estimated_hours": null,
            "custom_fields": [
                {
                    "id": 24,
                    "name": "client",
                    "value": ""
                }
            ],
            "created_on": "2020-01-09T12:29:00Z",
            "updated_on": "2020-01-09T12:51:42Z",
            "closed_on": null
        },
        {
            "id": 13,
            "project": {
                "id": 1,
                "name": "test_redmine"
            },
            "tracker": {
                "id": 1,
                "name": "Bug"
            },
            "status": {
                "id": 1,
                "name": "New"
            },
            "priority": {
                "id": 2,
                "name": "Normal"
            },
            "author": {
                "id": 141,
                "name": "sandeep@gmail.com sandeep@gmail.com"
            },
            "subject": "fortune PROBLEM11",
            "description": "",
            "start_date": "2020-01-09",
            "due_date": null,
            "done_ratio": 0,
            "is_private": false,
            "estimated_hours": null,
            "custom_fields": [
                {
                    "id": 24,
                    "name": "client",
                    "value": ""
                }
            ],
            "created_on": "2020-01-09T12:04:28Z",
            "updated_on": "2020-01-09T12:04:28Z",
            "closed_on": null
        },
        {
            "id": 12,
            "project": {
                "id": 1,
                "name": "test_redmine"
            },
            "tracker": {
                "id": 1,
                "name": "Bug"
            },
            "status": {
                "id": 1,
                "name": "New"
            },
            "priority": {
                "id": 2,
                "name": "Normal"
            },
            "author": {
                "id": 141,
                "name": "sandeep@gmail.com sandeep@gmail.com"
            },
            "subject": "fortune problem",
            "description": "",
            "start_date": "2020-01-09",
            "due_date": null,
            "done_ratio": 0,
            "is_private": false,
            "estimated_hours": null,
            "custom_fields": [
                {
                    "id": 24,
                    "name": "client",
                    "value": ""
                }
            ],
            "created_on": "2020-01-09T12:00:05Z",
            "updated_on": "2020-01-09T12:00:05Z",
            "closed_on": null
        },
        {
            "id": 11,
            "project": {
                "id": 1,
                "name": "test_redmine"
            },
            "tracker": {
                "id": 1,
                "name": "Bug"
            },
            "status": {
                "id": 1,
                "name": "New"
            },
            "priority": {
                "id": 2,
                "name": "Normal"
            },
            "author": {
                "id": 140,
                "name": "praveensharma@gmail.com praveensharma@gmail.com"
            },
            "subject": "fortune bug23",
            "description": "",
            "start_date": "2020-01-09",
            "due_date": null,
            "done_ratio": 0,
            "is_private": false,
            "estimated_hours": null,
            "custom_fields": [
                {
                    "id": 24,
                    "name": "client",
                    "value": ""
                }
            ],
            "created_on": "2020-01-09T11:28:46Z",
            "updated_on": "2020-01-09T11:28:46Z",
            "closed_on": null
        },
        {
            "id": 10,
            "project": {
                "id": 1,
                "name": "test_redmine"
            },
            "tracker": {
                "id": 1,
                "name": "Bug"
            },
            "status": {
                "id": 1,
                "name": "New"
            },
            "priority": {
                "id": 2,
                "name": "Normal"
            },
            "author": {
                "id": 140,
                "name": "praveensharma@gmail.com praveensharma@gmail.com"
            },
            "subject": "fortune bug",
            "description": "",
            "start_date": "2020-01-09",
            "due_date": null,
            "done_ratio": 0,
            "is_private": false,
            "estimated_hours": null,
            "custom_fields": [
                {
                    "id": 24,
                    "name": "client",
                    "value": ""
                }
            ],
            "created_on": "2020-01-09T10:51:48Z",
            "updated_on": "2020-01-09T10:51:48Z",
            "closed_on": null
        },
        {
            "id": 9,
            "project": {
                "id": 1,
                "name": "test_redmine"
            },
            "tracker": {
                "id": 1,
                "name": "Bug"
            },
            "status": {
                "id": 1,
                "name": "New"
            },
            "priority": {
                "id": 2,
                "name": "Normal"
            },
            "author": {
                "id": 138,
                "name": "shrilaljmallah888@gmail.com shrilaljmallah888@gmail.com"
            },
            "subject": "fortunekit issue",
            "description": "",
            "start_date": "2020-01-09",
            "due_date": null,
            "done_ratio": 0,
            "is_private": false,
            "estimated_hours": null,
            "custom_fields": [
                {
                    "id": 24,
                    "name": "client",
                    "value": ""
                }
            ],
            "created_on": "2020-01-09T10:46:25Z",
            "updated_on": "2020-01-09T10:46:25Z",
            "closed_on": null
        }
    ],
    "total_count": 6,
    "offset": 0,
    "limit": 25
}
*
 * @apiErrorExample {json}  Error 404:
 *     HTTP/1.1 404 NOT FOUND
 *{
    "message": "No data found"
}
*
 *
 */




/**
 * @api {post} /api/createClient Create new user and assign it to group.
*
 * @apiParam {String} AMName AM Name.
 * @apiParam {String} OMName OM Name.
 * @apiParam {String} emailID email.
 * @apiParam {String} password password.
 * @apiParam {String} clientName client name.

 * @apiGroup USER GROUP
 *
 * @apiParamExample {json} Body
 * 	{
    "emailID":"abc@gmail.com",
    "clientName":"abc",
    "password":"123456789012345",
    "AMName":"nisarg.g@loginextsolutions.com",
    "OMName":"nisarg.g@loginextsolutions.com"
}
 *  
 * @apiSuccessExample {json} Success.
 *    HTTP/1.1 201 CREATED
 *{
    "message": "user created"
}
 *
 * 
 *  @apiErrorExample {json}  Error 400:
 *     HTTP/1.1 400 Bad Request
 *     
 *{
    "error": "Invalid Email address or password length should be greater than 8"
}
 */




/**
 * @api {post} /api/addUsersThroughExcel Create new user and assign it to group thorugh excel.
*
 * @apiParam {Array} userDetails user details.
 * @apiParam {String} userDetails.email user email.
 * @apiParam {String} userDetails.group user.group.

 * @apiGroup USER GROUP
 *
 * @apiParamExample {json} Body
 *{
 	"userDetails": [{
        "email": "ram@gmail.com",
        "group":"firstblood"
     },{
        "email": "satish@gmail.com",
        "group":"firstblood"
    }]
}
 *  
 * @apiSuccessExample {json} Success.
 *    HTTP/1.1 201 CREATED
 *{
    "success": [
        {
            "email": "ram@gmail.com",
            "group": "firstblood"
        }
    ],
    "failure": [
        {
            "email": "satish@gmail.com",
            "group": "firstblood",
            "error": "user not found"
        }
    ]
}
 *
 *
 */


// API DOC FOR SOW


/**
 * @api {post} /api/createSubProject Create sub project for SOW.
*
 * @apiParam {String} name project name.
 * @apiParam {String} description description.
 * @apiParam {String} AMName AM name.
 * @apiParam {String} OMName OM name.


 * @apiGroup SOW
 *
 * @apiParamExample {json} Body
 * 	 {
	"name":"testing",
	"AMName":"kartik@gmail.com",
	"OMName":"kartik1@gmail.com",
	"description":"kartik sub project"
    }
 *  
 * @apiSuccessExample {json} Success.
 *    HTTP/1.1 201 CREATED
 *{
    "message": "sub project created"
}
 *
  * @apiErrorExample {json}  Error 422:
 *     HTTP/1.1 422 Unprocessable Entity
 *{
    "message": [
        "Identifier has already been taken"
    ]
}
 *
 */


/**
 * @api {get} /api/getSowIssue/:projectName/:userId?offset=3 Get all issues for watchers by its id.
 * @apiParam (params) {Id} userId user id
 * @apiParam (params) {String} projectName sub project name
 * @apiParam (query) {Number} offset offset for pagination
 * @apiGroup SOW
 * 
 * @apiSuccessExample {json} Success.
 *    HTTP/1.1 200 OK
 *{
    "issues": [
        {
            "id": 228,
            "project": {
                "id": 35,
                "name": "mailtesting"
            },
            "tracker": {
                "id": 1,
                "name": "Bug"
            },
            "status": {
                "id": 1,
                "name": "New"
            },
            "priority": {
                "id": 2,
                "name": "Normal"
            },
            "author": {
                "id": 1,
                "name": "Redmine Admin"
            },
            "assigned_to": {
                "id": 1,
                "name": "Redmine Admin"
            },
            "subject": "test mail2",
            "description": "",
            "start_date": "2020-03-02",
            "due_date": null,
            "done_ratio": 0,
            "is_private": false,
            "estimated_hours": null,
            "custom_fields": [
                {
                    "id": 24,
                    "name": "client",
                    "value": ""
                }
            ],
            "created_on": "2020-03-02T04:44:16Z",
            "updated_on": "2020-03-02T10:14:17Z",
            "closed_on": null
        },
        {
            "id": 227,
            "project": {
                "id": 35,
                "name": "mailtesting"
            },
            "tracker": {
                "id": 1,
                "name": "Bug"
            },
            "status": {
                "id": 1,
                "name": "New"
            },
            "priority": {
                "id": 2,
                "name": "Normal"
            },
            "author": {
                "id": 1,
                "name": "Redmine Admin"
            },
            "assigned_to": {
                "id": 1,
                "name": "Redmine Admin"
            },
            "subject": "test client 1",
            "description": "",
            "start_date": "2020-03-02",
            "due_date": null,
            "done_ratio": 0,
            "is_private": false,
            "estimated_hours": null,
            "custom_fields": [
                {
                    "id": 24,
                    "name": "client",
                    "value": "mailtesting"
                }
            ],
            "created_on": "2020-03-02T04:42:54Z",
            "updated_on": "2020-03-02T10:14:38Z",
            "closed_on": null
        }
    ],
    "total_count": 2,
    "offset": 0,
    "limit": 25
}
*
 * @apiErrorExample {json}  Error 404:
 *     HTTP/1.1 404 NOT FOUND
 *{
    "message": "no data found"
}
*
 *
 */


/**
 * @api {get} /api/sendMailForSOW/:id Send mail to all watchers.
 * @apiParam (params) {Id} id issue id
 * @apiGroup SOW
 * 
 * @apiSuccessExample {json} Success.
 *    HTTP/1.1 200 OK
 *{
    "message": "Mail Sent Successfully"
}
*
 *
 */


app.listen(8000, function () {
    console.log("Api up and running!");
});