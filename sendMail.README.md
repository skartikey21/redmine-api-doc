# SEND MAIL

## Send Mail to Author for all Issues (CRON JOB)

  File Name **SendMail.js**

  Function Name **SendMail**.

  - This function is for sending mail notification to author. while updating ticket with comment includes **@client**
  
  1. First get all the list of **issues** updated on last minute.
  2. Get **User** and **Journals** data from single issue in loop.
  3. Check that **User** data has user **email**. **Journals** includes **@client** which is created on last minute (This is to check multiple **@client** comment in last minute).
  4. Finally create mail template and send mail to **Author**.


## Send Mail to all Watchers for all SOW Issues (CRON JOB)

  File Name **SendMail.js**

  Function Name **SendMailForSow**.

  - This function is for sending mail notification to all Watchers. while updating ticket with comment includes **@client** and watchers role is **Reporter**.
  
  1. First get all the list of **SOW Issues** updated on last minute.
  2. Get **Watchers Email** and check Watchers role **Reporter** from single issue in loop. push watchers email into array.
  3. Check that **Project name** and custom field **client** should be equal. **Journals** includes **@client** which is created on last minute (This is to check multiple **@client** comment in last minute).
  4. Finally create mail template and send mail to all **Watchers**.


## Send Mail to all Watchers for all SOW New Issues (CRON JOB)

  File Name **SendMail.js**

  Function Name **SendMailForSowNewIssue**.

  - This function is for sending mail notification to all Watchers. while creating ticket with description includes **@client** and watchers role is **Reporter**.
  
  1. First get all the list of **SOW Issues** created on last minute.
  2. Check that **Project name** and custom field **client** should be equal. **Description** includes **@client**.
   2. Get **Watchers Email** and check Watchers role **Reporter** from single issue in loop. push watchers email into array.
  4. Finally create mail template and send mail to all **Watchers**.


## Send Mail to all Watchers for all SOW Issues By Given Issue Id through API.

  File Name **SendMail.js**

  Function Name **SendMailForSowIssue**.

  - This function is for sending mail notification to all Watchers. while updating ticket with comment includes **@client** and watchers role is **Reporter**.
  
  1. First get **SOW Issues** by issue id.
  2. Get **Watchers Email** and check Watchers role **Reporter** from single issue in loop. push watchers email into array.
  3. Check that **Project name** and custom field **client** should be equal. Take last created journal, in which
  includes **@client**.
  4. Finally create mail template and send mail to all **Watchers**.